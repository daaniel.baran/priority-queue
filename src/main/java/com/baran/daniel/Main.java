package com.baran.daniel;

public class Main {
    public static void main(String[] args) {
        QueueItem<String> item = new QueueItem<>(5, "1st");
        QueueItem<String> item1 = new QueueItem<>(5, "2nd");
        QueueItem<String> item2 = new QueueItem<>(3, "3rd");
        QueueItem<String> item3 = new QueueItem<>(3, "4th");
        QueueItem<String> item4 = new QueueItem<>(6, "5th");
        QueueItem<String> item5 = new QueueItem<>(6, "6th");

        PriorityQueue<String> queue = new PriorityQueue<>();
        queue.add(item1);
        queue.add(item2);
        queue.add(item4);
        queue.add(item3);
        queue.add(item);
        queue.add(item5);
        System.out.println(queue);
        System.out.println("peek: " + queue.peek());
        System.out.println("after peek: " + queue);
        System.out.println("pull: " + queue.pull());
        System.out.println("after pull: " + queue);
        System.out.println("peek: " + queue.peek());

    }
}