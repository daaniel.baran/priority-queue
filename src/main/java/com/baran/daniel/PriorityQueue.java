package com.baran.daniel;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class PriorityQueue<T> {

    public PriorityQueue() {
        this.items = new ArrayList<>();
    }

    private final List<QueueItem<T>> items;
    private QueueItem<T> head;

    public void add(QueueItem<T> item) {
        items.add(item);
        if (head == null || item.priority() > head.priority())
            head = item;
    }

    public QueueItem<T> peek() {
        return head;
    }

    public QueueItem<T> pull() {
        if (head != null) {
            QueueItem<T> item = head;
            removeHighestPriorityItem();
            return item;
        }
        throw new NoSuchElementException("No elements, list is empty.");
    }

    public boolean isEmpty() {
        return items.isEmpty();
    }

    public int size() {
        return items.size();
    }

    private void removeHighestPriorityItem() {
        this.items.remove(head);
        this.head = findHighestPriority();
    }

    private QueueItem<T> findHighestPriority() {
        if (!items.isEmpty()) {
            QueueItem<T> highest = null;

            for (QueueItem<T> item : this.items) {
                if (highest == null || item.compareTo(highest) > 0) {
                    highest = item;
                }
            }
            return highest;
        }
        throw new NoSuchElementException("No elements, list is empty.");
    }

    @Override
    public String toString() {
        return "head=" + head + ", items = " + items;
    }
}

record QueueItem<T>(int priority, T item) implements Comparable<QueueItem<T>> {
    @Override
    public int compareTo(QueueItem<T> item) {
        return Integer.compare(priority, item.priority());
    }

    @Override
    public String toString() {
        return "{ priority: " + priority + ", item: " + item + " }";
    }
}